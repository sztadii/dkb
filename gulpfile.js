'use strict';

var $ = require('gulp-load-plugins')();
var gulp = require('gulp');
var cleancss = require('gulp-clean-css');
var rimraf = require('gulp-rimraf');
var ignore = require('gulp-ignore');
var htmlmin = require('gulp-htmlmin');
var gcmq = require('gulp-group-css-media-queries');
var argv = require('yargs').argv;
var browser = require('browser-sync').create();
var panini = require('panini');
var sequence = require('run-sequence');

// Check for --production flag
var isProduction = !!(argv.production);

// Port to use for the development server.
var PORT_LOCAL = 8000;
var PORT_UI = 3000;

// Browsers to target when prefixing CSS.
var COMPATIBILITY = ['last 2 versions', 'ie >= 9'];

// File paths to various assets are defined here.
var PATHS = {
    assets: [
        'src/assets/**/*',
        '!src/assets/{!img,js,sass,css}{,/**/*}'
    ],
    css: [
        'src/assets/css/main-sass.css'
    ],
    javascript: [
        'bower_components/jquery/jquery.js',
        'src/bundles/slick/_slick.js',
        'src/assets/js/_vars.js',
        'src/assets/js/_helpers.js',
        'src/assets/js/main-js.js'
    ]
};

// Delete the "dist" folder
// This happens every time a build starts
gulp.task('clean', function () {
    return gulp.src('dist/', {read: false})
        .pipe(rimraf());
});

gulp.task('clean:img', function () {
    return gulp.src('dist/assets/img/*', {read: false})
        .pipe(rimraf());
});

gulp.task('clean:pages', function () {
    return gulp.src('dist/**.html', {read: false})
        .pipe(ignore('dist/assets'))
        .pipe(rimraf());
});

// Copy files out of the assets folder
// This task skips over the "img", "js", and "sass" folders, which are parsed separately
gulp.task('copy', function () {
    gulp.src(PATHS.assets)
        .pipe(gulp.dest('dist/assets'));
});

// Copy currentSection templates into finished HTML files
gulp.task('pages', function () {
    var options = {
        collapseWhitespace: true,
        minifyCSS: true,
        minifyJS: true,
        removeComments: true
    };
    var minify = $.if(isProduction, htmlmin(options));

    gulp.src('src/pages/**/*.{html,hbs,handlebars}')
        .pipe(panini({
            root: 'src/pages/',
            layouts: 'src/layouts/',
            partials: 'src/partials/',
            data: 'src/data/'
        }))
        .pipe(minify)
        .pipe(gulp.dest('dist'));
});

gulp.task('pages:reset', function () {
    panini.refresh();
    gulp.run('pages');
});

// Compile Sass into CSS
gulp.task('sass', function () {

    return gulp.src('src/assets/sass/*.sass')
        .pipe($.sass().on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: COMPATIBILITY
        }))
        .pipe(gulp.dest('src/assets/css'));
});

// Concat all css
// In production, the file is minified
gulp.task('css', function () {
    var cssOptions = {
        keepSpecialComments: 0 //Remove special comments
    };

    return gulp.src(PATHS.css)
        .pipe($.concat('app.css'))
        .pipe($.if(isProduction, gcmq()))
        .pipe($.if(isProduction, cleancss(cssOptions)))
        .pipe(gulp.dest('dist/assets/css'));
});

// Run tast sass, css
gulp.task('styles', function (done) {
    sequence('sass', ['css'], done);
});

// Combine JavaScript into one file
// In production, the file is minified
gulp.task('javascript', function () {
    var uglify = $.if(isProduction, $.uglify()
        .on('error', function (e) {
            console.log(e);
        }));

    return gulp.src(PATHS.javascript)
        .pipe($.if(!isProduction, $.sourcemaps.init()))
        .pipe($.concat('app.js'))
        .pipe(uglify)
        .pipe($.if(!isProduction, $.sourcemaps.write()))
        .pipe(gulp.dest('dist/assets/js'));
});

// Copy images to the "dist" folder
// In production, the images are compressed
gulp.task('images', function () {
    var imagemin = $.if(isProduction, $.imagemin({
        progressive: true
    }));

    return gulp.src('src/assets/img/**/*')
        .pipe(imagemin)
        .pipe(gulp.dest('dist/assets/img'));
});

// Build the "dist" folder by running all of the above tasks
gulp.task('build', function (done) {
    sequence('clean', ['pages', 'styles', 'javascript', 'images', 'copy'], done);
});

// Start a server with LiveReload to preview the site in
gulp.task('server', ['build'], function () {
    browser.init({
        server: 'dist',
        reloadDelay: 1000,
        port: PORT_LOCAL,
        open: 'local',
        ui: {
            port: PORT_UI
        },
        plugins: ['bs-latency']
    });
});

// Build the site, run the server, and watch for file changes
gulp.task('default', ['build', 'server'], function () {
    gulp.watch(PATHS.assets, ['copy', browser.reload]);
    gulp.watch(['src/pages/**/*.html'], ['clean:pages', 'pages', browser.reload]);
    gulp.watch(['src/{layouts,partials}/**/*.html'], ['pages:reset', browser.reload]);
    gulp.watch(['src/bundles/**/*'], ['styles', 'javascript', browser.reload]);
    gulp.watch(['src/data/**/*'], ['clean:pages', 'pages:reset', browser.reload]);
    gulp.watch(['src/assets/sass/**/*.sass'], ['sass']);
    gulp.watch(PATHS.css, ['styles', browser.reload]);
    gulp.watch(['src/assets/js/**/*.js'], ['javascript', browser.reload]);
    gulp.watch(['src/assets/img/**/*'], ['clean:img', 'images', browser.reload]);
});