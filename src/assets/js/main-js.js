$(document).ready(function () {

    windowWidth = window.innerWidth;

    shortText('.gallery-info > p', 80);
    shortText('#about > p', 600);
    shortText('.news-item > p', 80);
    shortText('.other-proposition > a', 80);
    shortText('.content-news-info > p', 120);

    clickButton('.nav-icon1', '#menu-box');
    clickSideMenu();

    makeMainSlider();
    makeTrumpSlider();
    makeNewsSlider();
    makeWorkSlider();
    makeContentNewsSlider();

    moveArrows();
    moveToTop();

    setTimeout(function(){
        moveMagicLine('#menu-box', '#current_page_item', 'active', '#magic-line');
    }, 300);

    resizeWorkSlider();

    pulsAnimate('#other-questions', 800, 0.4, 1);

});

$(window).resize(function () {

    windowWidth = window.innerWidth;

    moveMagicLine('#menu-box', '#current_page_item', 'active', '#magic-line');

    resizeWorkSlider();

});

$(window).scroll(function () {

    screenBottom = $(window).scrollTop() + $(window).height();

    landingEachElements('.gallery-img .button1', '#gallery-box', 0.9);
    landingEachElements('.content-news-item .button3', '#content-news', 0.1, 200);
    landingElement('#about', '#about-box', 0.6);
    landingElement('#news', '#news-box', 0.6);
    landingElement('#contact', '#contact-box', 0.6);
    landingElement('#work', '#content-box', 0.75);

    fadingElement('.slider-item .container-in');
    fadingNumber();

});