//CLICKING FUNCTIONS region

function clickButton(_button, _object) {
    var button = $(_button),
        object = $(_object);

    if (button.length && object.length) {
        button.click(function () {
            object.slideToggle();
        });
    }
}

function clickSideMenu() {
    $('li').on('click', function () {
        $(this).addClass('show-li').siblings().removeClass('show-li');
    });
}

//endregion

//MAKING FUNCTIONS region

function makeMainSlider() {
    $('#slider-main').slick({
        arrows: false,
        dots: false,
        fade: true,
        infinite: false,
        cssEase: 'linear',
        autoplay: true
    });

    makeNewArrows();
}

function makeNewArrows() {
    var arrowNext = $('#arrow-next'),
        arrowPrev = $('#arrow-prev'),
        slider = $('#slider-main');

    arrowNext.click(function () {
        slider.slick('slickNext');
    });

    arrowPrev.click(function () {
        slider.slick('slickPrev');
    });
}

function makeNewsSlider() {
    $('#news').slick({
        dots: false,
        arrows: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: false,
        //speed: 500,
        cssEase: 'linear',
        autoplay: true,
        //autoplaySpeed: 2500,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 900,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
}

function makeTrumpSlider() {
    $('#trump-slider').slick({
        arrows: false,
        dots: false,
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        //speed: 500,
        autoplay: true,
        //aotoplaySpeed: 2500,
        cssEase: 'linear',
        //asNavFor: '#slider-main',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
}

function makeWorkSlider() {
    $('#work-slider').slick({
        dots: false,
        arrows: false,
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        //speed: 500,
        cssEase: 'linear',
        autoplay: true,
        //autoplaySpeed: 2500,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
}

function makeContentNewsSlider() {
    $('#content-news').slick({
        arrows: false,
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        autoplay: true,
        vertical: true,
        cssEase: 'linear'
    });
}

//endregion

//MOVING FUNCTIONS region

function moveArrows() {
    var arrow1 = $('#arrow-next'),
        arrow2 = $('#arrow-prev'),
        sliderBox = $('#slider-box'),
        slider = $('#slider-main');

    sliderBox.delegate(slider, 'mouseover mouseleave', function (e) {

        if (e.type == 'mouseover') {
            arrow1.addClass('arrow-on-place');
            arrow2.addClass('arrow-on-place');
        } else {
            arrow1.removeClass('arrow-on-place');
            arrow2.removeClass('arrow-on-place');
        }
    });
}

function moveToTop() {
    var buttonUp = $("#button-up-box");

    buttonUp.click(function (e) {
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });
}

function moveMagicLine(_menu, _currentPageItem, _active, _line) {
    var menuBox = $(_menu);

    if (windowWidth >= PC && menuBox.length) {

        if (!$(_line).length) {
            var tmp = _line.replace("#", "");

            if (_line[0] == "#")
                menuBox.append("<li id=" + tmp + "></li>");

            else if (_line[0] == ".")
                menuBox.append("<li class=" + tmp + "></li>");
        }

        var magicLine = $(_line),
            currentPageItem = $(_currentPageItem),
            currentPageAnchor = currentPageItem.find('a'),
            el, leftPos, newWidth;

        magicLine
            .width($(_currentPageItem).width())
            .css('left', currentPageAnchor.position().left)
            .data('origLeft', magicLine.position().left)
            .data('origWidth', magicLine.width());

        currentPageAnchor.addClass(_active);

        menuBox.find('li a').hover(function () {
            el = $(this);
            leftPos = el.position().left;
            newWidth = el.parent().width();

            magicLine.stop().animate({
                left: leftPos,
                width: newWidth
            });

            menuBox.find('a').removeClass(_active);
            el.addClass(_active);

        }, function () {
            magicLine.stop().animate({
                left: magicLine.data('origLeft'),
                width: magicLine.data('origWidth')
            });

            $(this).removeClass(_active);
            currentPageAnchor.addClass(_active);
        });
    } else if (windowWidth < PC && menuBox.length) {
        $(_line).remove();
    }
}

//endregion

//RESIZE FUNCTIONS region

function resizeWorkSlider() {
    var workSlider = $('#work-slider'),
        contentMarginRight = parseInt($('#content').css('margin-right')),
        parentWidth = $('.container-in').width() - $('#side-box').width() - contentMarginRight;

    if (parentWidth) {
        workSlider.css({
            'width': ' ' + parentWidth + 'px '
        });
    } else {
        workSlider.css({
            'width': '100%'
        });
    }
}

//endregion

//LANDING FUNCTION region

function landingElement(_object, _container, _height) {
    var object1 = $(_object),
        container = $(_container),
        height = _height;

    if (windowWidth >= PC && object1.length) {
        var triggerPosition = container.offset().top + container.outerHeight() * height;

        if (screenBottom > triggerPosition) {
            object1.addClass('showing');
        }
    }
}

function landingEachElements(_objects, _container, _height) {
    var objects = $(_objects),
        container = $(_container),
        height = _height;

    if (windowWidth >= PC && objects.length) {
        var triggerPosition = container.offset().top + container.outerHeight() * height;

        if (screenBottom > triggerPosition) {
            objects.each(function (i) {
                setTimeout(function () {
                    objects.eq(i).addClass('showing');
                }, 200 * (i + 1));
            });
        }
    }
}

//endregion

//FADING FUNCTIONS region

function fadingElement(_object) {
    var object = $(_object);

    if (windowWidth >= PC && object.length) {
        var wScroll = $(window).scrollTop(),
            value = 100 / (wScroll + 20);

        object.css({
            'opacity': value <= 1 ? value : 1
        });
    } else {
        object.css({
            'opacity': '1'
        });
    }
}

function fadingNumber() {
    var object1 = $('#other-right'),
        object2 = $('#button-up-box'),
        container = $('#other-box'),
        height = 0.75;

    if (windowWidth >= PC && object1.length && object2.length) {
        var screenBottom = $(window).scrollTop() + $(window).height(),
            triggerPosition = container.offset().top + container.outerHeight() * height;

        if (screenBottom > triggerPosition) {
            object1.addClass('showing');
            object2.addClass('showing');
        }
    }
}

function pulsAnimate(_object, time, start, end) {
    var object = $(_object);

    if (object.length) {

        $(object).animate({
            'opacity': '' + start + ''
        }, time, function () {
            $(this).animate({
                'opacity': '' + end + ''
            }, time, makePuls());
        });
    }
}

//endregion

//TESTING FUNCTION region

function isMobile() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent))
        return true;

    return false;
}

//endregion

//TEXT FUNCTIONS region

function shortText(_object, _maxLength) {
    var object = $(_object);

    if (object.length) {
        object.each(function () {
            var element = $(this),
                elementText = element.text().trim(),
                shortText;

            if (elementText.length > _maxLength) {

                shortText = elementText.substr(0, _maxLength);

                var arg1 = _maxLength,
                    arg2 = shortText.lastIndexOf(" "),
                    mathMin = Math.min(arg1, arg2);

                if (isSpacecialCharacter(shortText[mathMin - 1]))
                    --mathMin;

                shortText = shortText.substr(0, mathMin) + '...';
            }

            else {
                shortText = elementText.substr(0, _maxLength);
            }

            element.text(shortText);
        });
    }
}

function isSpacecialCharacter(_character) {
    if (/^[a-zA-Z0-9- ]*$/.test(_character) == false) {
        return true;
    }

    return false;
}

//endregion